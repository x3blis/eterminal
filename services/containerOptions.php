<?php
//ini_set('display_errors', '1');
require('config.php');

$mysqli = new mysqli($gaSql['server'], $gaSql['user'], $gaSql['password'], $gaSql['db']);

    if ($mysqli->connect_error) {
        die('Connection error (' . $mysqli->connect_errno . ') '
                . $mysqli->connect_error);
    }
    
$mysqli->query('SET NAMES UTF8');
 
$options = array('owner', 'depot', 'capacity', 'type', 'state', 'damage', 'size');
//var_dump($options);
$out = array();
foreach($options as $option)
{
    $query = 'SELECT name FROM '.$option.' WHERE enable=1 ORDER BY name';
    $result = $mysqli->query($query);
    while($row = $result->fetch_assoc())
    {
        $out[$option][] = $row['name'];
        //echo $row['name'];
    }
}

$p_query = "SELECT DISTINCT position FROM container WHERE out_time = 0 AND position <> '' ORDER BY position";
$p_result = $mysqli->query($p_query);
while($p_row = $p_result->fetch_assoc())
{
    $out['position'][] = $p_row['position'];
}
 
echo json_encode($out); 
 
?>