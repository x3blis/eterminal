<?php
date_default_timezone_set('Europe/Moscow');
ini_set('display_errors', '1');
   /*
    * Script:    DataTables server-side script for PHP and MySQL
    * Copyright: 2010 - Allan Jardine
    * License:   GPL v2 or BSD (3-point)
    */ 
    
require('config.php');
   
   $aColumns = array( 
                "container.id AS id",
                "container",
                "owner.name AS company",
                "position",
                "state.name AS state",
                "damage.name AS damage",
                "type.name AS type",
                "capacity.name AS capacity",
                "size.name AS size",         
                "in_time",           
                "out_time",
                "comment",
                "in_act",
                "in_railway",
                "in_station",
                "in_platform",
                "in_car",
                "in_driver",
                "in_carrier",
                "in_codeco",
                "container.return AS ret",     
                "out_act",
                "out_railway",
                "out_station",
                "out_platform",
                "out_car",
                "out_driver",
                "out_carrier",
                "out_codeco",
                "container.release AS rel",
                "file",
                "package",
                "container.name AS manager",                
                "row",
                "floor",                
                "container.enable AS enable",
                "depot.name AS depot"
           		
        );
   
    
   /* Indexed column (used for fast and accurate table cardinality) */
   $sIndexColumn = "id";
 
   /* DB table to use */
   $sTable = "container";
 
   // Joins
   
   $sJoin =  'LEFT JOIN `depot` ON `container`.`depot`=`depot`.`id` ';
   $sJoin .= 'LEFT JOIN `size` ON `container`.`size`=`size`.`id` ';
   $sJoin .= 'LEFT JOIN `type` ON `container`.`type`=`type`.`id` ';
   $sJoin .= 'LEFT JOIN `state` ON `container`.`state`=`state`.`id` ';
   $sJoin .= 'LEFT JOIN `owner` ON `container`.`owner`=`owner`.`id` ';
   $sJoin .= 'LEFT JOIN `damage` ON `container`.`damage`=`damage`.`id` ';
   $sJoin .= 'LEFT JOIN `capacity` ON `container`.`capacity`=`capacity`.`id` ';

    
   // get or post
   $_METHOD = $_POST;
   
   /* MySQL connection */
   /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    * If you just want to use the basic configuration for DataTables with PHP server-side, there is
    * no need to edit below this line
    */
   $mysqli = new mysqli($gaSql['server'], $gaSql['user'], $gaSql['password'], $gaSql['db']);

    if ($mysqli->connect_error) {
        die('Connection error (' . $mysqli->connect_errno . ') '
                . $mysqli->connect_error);
    }
    
   $mysqli->query('SET NAMES UTF8');
 
   $sLimit = "LIMIT 100";
   if ( isset( $_METHOD['iDisplayStart'] ) && $_METHOD['iDisplayLength'] != '-1' )
   {
     $sLimit = "LIMIT ".$mysqli->real_escape_string( $_METHOD['iDisplayStart'] ).", ".
       $mysqli->real_escape_string( $_METHOD['iDisplayLength'] );
   }
   
   $sOrder = "ORDER BY id DESC";
 
   if ( isset( $_METHOD['iSortCol_0'] ) )
   {
     $sOrder = "ORDER BY  ";
     for ( $i=0 ; $i<intval( $_METHOD['iSortingCols'] ) ; $i++ )
     {
       if ( $_METHOD[ 'bSortable_'.intval($_METHOD['iSortCol_'.$i]) ] == "true" )
       {
         $sFieldname = $aColumns[ intval( $_METHOD['iSortCol_'.$i] ) ];
         if(strpos($sFieldname, " AS ") !== false) {
           $arr = explode(" ", $sFieldname);
           $sFieldname = $arr[0];
         }         
         $sOrder .= $sFieldname . "
           ".$mysqli->real_escape_string( $_METHOD['sSortDir_'.$i] ) .", ";
       }
     }
 
     $sOrder = substr_replace( $sOrder, "", -2 );
     if ( $sOrder == "ORDER BY" )
     {
       $sOrder = "";
     }
   }
 
   $sWhere = "";
   if ( isset($_METHOD['sSearch'])&&$_METHOD['sSearch'] != "" )
   {
     $sWhere = "WHERE (";
     for ( $i=0 ; $i<count($aColumns) ; $i++ )
     {
       $sFieldname = $aColumns[$i]; // iptv_uitgezet.username AS iptv_uitgezet
       if(strpos($sFieldname, " AS ") !== false) {
         $arr = explode(" ", $sFieldname);
         $sFieldname = $arr[0];
       }
       $sWhere .= $sFieldname." LIKE '%".$mysqli->real_escape_string( $_METHOD['sSearch'] )."%' OR ";
     }
     $sWhere = substr_replace( $sWhere, "", -3 );
     $sWhere .= ')';
   }
 
   /* Individual column filtering */
   for ( $i=0 ; $i<count($aColumns) ; $i++ )
   {
     if ( isset($_METHOD['bSearchable_'.$i])&&$_METHOD['bSearchable_'.$i] == "true" && $_METHOD['sSearch_'.$i] != '' )
     {
       if ( $sWhere == "" )
       {
         $sWhere = "WHERE ";
       }
       else
       {
         $sWhere .= " AND ";
       }
       $sFieldname = $aColumns[$i];
       if(strpos($sFieldname, " AS ") !== false) {
         $arr = explode(" ", $sFieldname);
         $sFieldname = $arr[0];
       }
       $sWhere .= $sFieldname." LIKE '%".$mysqli->real_escape_string($_METHOD['sSearch_'.$i])."%' ";
     }
   }
   
   if ( isset($_METHOD['events_period'])&&$_METHOD['events_period'] != "" )
   {
       $sDates = explode('~', $_METHOD['events_period']);
        $sWhere = ' WHERE in_time BETWEEN '.$sDates[0].' AND '.$sDates[1].' OR out_time BETWEEN '.$sDates[0].' AND '.$sDates[1].' ';
   }
   /*
    * SQL queries SELECT SQL_CALC_FOUND_ROWS `".str_replace(" , ", " ", implode("`, `", $aColumns))."`
    * Get data to display
    */
   $sQuery = "
     SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
     FROM   $sTable
     $sJoin
     $sWhere
     $sOrder
     $sLimit
   ";
   $sDataQuery = $sQuery;
   //echo $sQuery;
   //$rResult = mysql_query( $sQuery, $gaSql['link'] ) or die(mysql_error());
   $rResult = $mysqli->query($sQuery);
   
 
   /* Data set length after filtering */
   $sQuery = "
     SELECT FOUND_ROWS()
   ";
   $rResultFilterTotal = $mysqli->query( $sQuery);
   $aResultFilterTotal = $rResultFilterTotal->fetch_array();
   $iFilteredTotal = $aResultFilterTotal[0];
 
   /* Total data set length */
   $sQuery = "
     SELECT COUNT(".$sIndexColumn.")
     FROM   $sTable
   ";
   $rResultTotal = $mysqli->query( $sQuery);
   $aResultTotal = $rResultTotal->fetch_array();  
   $iTotal = $aResultTotal[0];
 
 
   /*
    * Output
    */
    if(!isset($_METHOD['sEcho'])) $_METHOD['sEcho']=0;
    
   $output = array(
     "sEcho" => intval($_METHOD['sEcho']),
     "iTotalRecords" => $iTotal,
     "iTotalDisplayRecords" => $iFilteredTotal,
     "aaData" => array(),
     "sQuery" => $sDataQuery
   );
 
   while ( $aRow = $rResult->fetch_assoc() )
   {
     $row = array();
     
     for ( $i=0 ; $i<count($aColumns) ; $i++ )
     {
       if(strpos($aColumns[$i], " AS ") !== false) {
         $arr = explode(" ", $aColumns[$i]);
         $row[$arr[2]] = $aRow[ $arr[2] ];
       }
       else if ( $aColumns[$i] == "version" )
       {
         /* Special output formatting for 'version' column */
         $row[] = ($aRow[ $aColumns[$i] ]=="0") ? '-' : $aRow[ $aColumns[$i] ];
       }
       else if ( $aColumns[$i] != ' ' )
       { 
         $row[$aColumns[$i]] = $aRow[ $aColumns[$i] ]; // truncate long results
       }

     }
     $output['aaData'][] = $row;
   }
 //header('Content-Type: application/json');
   echo json_encode( $output );
  //header('Content-Type: application/javascript');
   //echo $_GET['callback'].'('.json_encode( $output ).');';
   exit();
 
?>
