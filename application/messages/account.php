<?php defined('SYSPATH') or die('No direct access allowed.');
 
return array(
    'success' => 'Password reset email sent.',
    'failure' => 'Could not send email.',
    
);