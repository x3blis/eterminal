<?php defined('SYSPATH') or die('No direct access allowed.');

return array(
	'encrypt_key' => 'fnvhytavsbfkl', 
	'cookie_salt' => ',vblvobjkrfnnrjkdhsv..amajduu7',
	'cookie_lifetime' => 86400,
	'session_lifetime' => 0,
	'header' => array
	(
		'title' => 'eTerminal',
		'keyword' => 'eTerminal',
	),
	'language' => array
	(
			'en',
			'ru',
	),
	'account'	=> array
	(
		'create'	=> array
		(
			'username'	=> array
			(
				'min_length'	=> 2,
				'max_length'	=> 12,
				'format'		=> 'alpha_numeric',
			),
			'password'	=> array
			(
				'min_length'	=> 6,
			)
		),
	),
);
