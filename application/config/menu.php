<?php defined('SYSPATH') or die('No direct script access.');

return array(
	
	'terminal' => 'Terminal',
    'arrival' => 'Arrival',
    'requests' => array(	
		'requests'  => 'In',
		'out' => 'Out'
	),
    'clients' => array(    
		'clients'  => 'Clients',
		'contracts' => 'Contracts',
        'invoices' => 'Invoices'
	),
    
    'repair' => 'Repair',
	'log'   => 'Log',
	'stat'   => 'Stat',
	'chat'      => 'Chat',

);
