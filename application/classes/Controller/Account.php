<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Account extends Controller_Template_Basic {

    public function before()
	{
		parent::before();
	}


	// nothing here
	public function action_index()
	{
		$this->redirect();
	}


	// create account
	public function action_create()
	{
       
		// form post handling
		if (isset($_POST) && Valid::not_empty($_POST)) {

			// validate
			$post = Validation::factory($_POST)
			->rule('email', 'not_empty')
			->rule('email', 'email')
			->rule('email', 'email_domain');			

			if ($post->check()) {
				// save
				$model = ORM::factory('User');
                $pwd = $this->generate_token();
				$model->values(array(
					'email'		=> $post['email'],
					'username' 	=> $post['email'],//HTML::entities(strip_tags($post['username'])),
					'password'	=> $pwd,//$post['password'],
                    'reset_token' => $this->generate_token(32),
				));
				try {
					$model->save();

					$model->add('roles', ORM::factory('Role')->where('name', '=', 'login')->find());
					$model->add('roles', ORM::factory('Role')->where('name', '=', 'participant')->find());
					// success login
                    $message = "You can compete registration by visiting the page at:\n\n" .
    							           ":reset_token_link\n\n";
				// Create complex Swift_Message object stored in $message
				// MUST PASS ALL PARAMS AS REFS
				$subject = __('Registration');
				$to = $post['email'];
				$from = Kohana::$config->load('useradmin')->email_address;
				$body = __($message, array(
									':reset_token_link' => URL::site('account/password?token='.$model->reset_token.'&email='.$post['email'], TRUE), 
									
				));
				// FIXME: Test if Swift_Message has been found.
				$message_swift = Swift_Message::newInstance($subject, $body)->setFrom($from)->setTo($to);

                if ($this->sendmail($to, $subject, $body))
				{
					//Message::add('success', __('Password reset email sent.'));
                    $sent = 1;
					$this->redirect('account/login');
				}
					
				}
				catch (ORM_Validation_Exception $e) {
					$errors = $e->errors('user');
				}
			} else {
				$errors = $post->errors('user');
			}
		}

		// TODO i18n
		$this->template->title = __('Create an account');

		// display
		$this->template->content = View::factory('account/create')
		->bind('post', $post)
		->bind('errors', $errors);
	}


	// login
	public function action_login()
	{
		// user already logged in, redirect to dashboard
		if (Auth::instance()->logged_in('participant')) {
			$this->redirect('terminal');
		}

		// received the POST
		if (isset($_POST) && Valid::not_empty($_POST)) {

			// validate the login form
			$post = Validation::factory($_POST)
			->rule('username', 'not_empty')
			->rule('password', 'not_empty')
			->rule('password', 'min_length', array(':value', 3));
			$remember = isset($post['remember']);

			//TODO use email or username login

			// if the form is valid and the username and password matches
			if ($post->check() && Auth::instance()->login($post['username'], $post['password'], $remember))
			{
				if(Auth::instance()->logged_in('participant')) {
					// sucessfully loged
					$this->redirect('terminal');
				}
			} else {
				// wrong username or password (but form is valid)
				$loginerrors =  __('Wrong username or password');
			}
			// validation failed, collect the errors
			$errors = $post->errors('user');
		}

		// display
		$this->template->title = __('Login');
		$this->template->content = View::factory('account/login')
			->bind('post', $post)
			->bind('errors', $errors)
			->bind('loginerrors', $loginerrors);
	}


	// login - help
	public function action_help()
	{
		// display
		$this->template->title = 'Help';
		$this->template->content = View::factory('account/help');
	}
    private function sendmail($to, $subject, $message){
        $config = Kohana::$config->load('email');
        $from = $config->login;
        $server = $config->smtp_server;
        $port = $config->port;
        $login = $config->login;
        $pass = $config->password;
        $transport = Swift_SmtpTransport::newInstance($server, $port)
          ->setUsername($login)
          ->setPassword($pass)
          ;
  		
  		//Create the Mailer using your created Transport
  		$mailer = Swift_Mailer::newInstance($transport);
         
        $message_swift = Swift_Message::newInstance($subject, $message)->setFrom($from)->setTo($to);
        if ($mailer->send($message_swift))
    			{
					return TRUE;
				}
				else
				{
					return FALSE;
				}
    }

	// login - reset step 1
	public function action_reset()
	{
		// received the POST
		if (isset($_POST) && Valid::not_empty($_POST)) {		
			// validate the login form
			$post = Validation::factory($_POST)
			->rule('email', 'not_empty')
			->rule('email', 'email')
			->rule('email', 'email_domain');
			//->rule('email', array($this, 'pwdexist'), array(':validation', ':field')); //TODO duplicate of ORM::factory('User') below

			// if the form is valid and the username and password matches
			if ($post->check()) {

				$user = ORM::factory('User')->where('email', '=', $post['email'])->find();
				$user->reset_token = $this->generate_token(32);
				$user->save();	
  				
				$message = "You have requested a password reset. You can reset password to your account by visiting the page at:\n\n" .
								           ":reset_token_link\n\n" .
								           "If the above link is not clickable, please visit the following page:\n" .
								           ":reset_link\n\n" .
								           "and copy/paste the following Reset Token: :reset_token\nYour user account name is: :username\n";
				// Create complex Swift_Message object stored in $message
				// MUST PASS ALL PARAMS AS REFS
				$subject = __('Account password reset');
				$to = $_POST['email'];
				$from = Kohana::$config->load('useradmin')->email_address;
				$body = __($message, array(
									':reset_token_link' => URL::site('account/password?token='.$user->reset_token.'&email='.$_POST['email'], TRUE), 
									':reset_link' => URL::site('account/reset', TRUE), 
									':reset_token' => $user->reset_token, 
									':username' => $user->username
				));
				// FIXME: Test if Swift_Message has been found.
				$message_swift = Swift_Message::newInstance($subject, $body)->setFrom($from)->setTo($to);

                if ($this->sendmail($to, $subject, $body))
				{
					//Message::add('success', __('Password reset email sent.'));
                    $sent = 1;
					//$this->redirect('account/login');
				}				
				
				
			} else {
				// validation failed, collect the errors
				$errors = $post->errors('user');
			}
		}

		// display
        
        $this->template->title = 'Reset password';
		$this->template->content = View::factory('account/reset')
			->bind('post', $post)
			->bind('errors', $errors)
			->bind('sent', $sent);
	}


	// login - reset step 2
	public function action_password()
	{
		// user already logged in, redirect to dashboard
		if (Auth::instance()->logged_in('participant')) {
			$this->redirect('dashboard');
		}

		// try to match
		if (isset($_GET['token']) && isset($_GET['email'])) {
			if ((strlen($_GET['token']) == 32) && Valid::email($_GET['email'])) {
				// match $_GET with user
				$user = ORM::factory('User')->where('email', '=', $_GET['email'])->where('reset_token', '=', $_GET['token'])->find();
				if($user->loaded()) {
					$found = 1;
                    $pwd = $this->generate_token();
                    $user->reset_token = NULL;
        			$user->password = $pwd;
    				$user->save();
				} else {
					$found = 0;                   
				}
			} else {
				$this->redirect();
			}
		} else {
			$this->redirect();
		}


		// display
		$this->template->title = 'Reset password step 2';
		$this->template->content = View::factory('account/password')
			->bind('pwd', $pwd)
			->bind('found', $found);
	}


	// check if username is available
	// call by ajax
	public function action_checkusername()
	{
		
		$this->auto_render = FALSE;

		if(!ORM::factory('User')->unique_key_exists($_POST['email'])) {
			echo json_encode(array('available' => 1));
		} else {
			echo json_encode(array('available' => 0));
		}
		
	}


	// validation rule: password != username
	static function pwdneusr($validation, $password, $username)
	{
		if ($validation[$password] === $validation[$username])
		{
			$validation->error($password, 'pwdneusr');
		}
	}


	//validation rule: password exist
	static function pwdexist($validation, $email)
	{
		if(!ORM::factory('User')->unique_key_exists($validation[$email])) {
			$validation->error($email, 'emailexistnot');
		}
	}


	// generate token
	static function generate_token($length = 8)
	{
		// start with a blank password
		$password = "";
		// define possible characters (does not include l, number relatively likely)
		$possible = "123456789abcdefghjkmnpqrstuvwxyz123456789";
		// add random characters to $password until $length is reached
		for ($i = 0; $i < $length; $i++) {
			// pick a random character from the possible ones
			$char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
			$password .= $char;
		}
		return $password;
	}
    
    public function action_language()
    {
		// requested language
		$lang = $this->request->param('id');

		// security
		if(!isset($lang) || empty($lang)) {
			$this->redirect('terminal');
		}
		
		if(!in_array($lang, Kohana::$config->load('eterminal.language'))) {
			$lang = 'en';
		}
		
		Cookie::set('lang', $lang);
		I18n::lang($lang);
        
        if (Auth::instance()->logged_in('participant'))
		$this->redirect(Session::instance()->get('controller'));
        //$this->redirect('terminal');
	}
    
    // participant logout
    public function action_logout()
	{
		// log user out
		Auth::instance()->logout();
		// redirect to login page
		$this->redirect('');
	}

}