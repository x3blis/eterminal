<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Requests extends Controller_Template_Basic {

	public function action_index()
	{
		$this->template->title = __('Requests');
  		$this->template->content = View::factory('requests/in');
	}
}
