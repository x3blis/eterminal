<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Out extends Controller_Template_Basic {

	public function action_index()
	{
		$this->template->title = __('Out');
  		$this->template->content = View::factory('requests/out');
	}
    
}
