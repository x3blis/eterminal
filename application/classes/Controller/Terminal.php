<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Terminal extends Controller_Template_Basic {


    public function before()
    {
		parent::before();
		
		if (!Auth::instance()->logged_in('participant'))
		{
			$this->redirect('account/login');
		}
       
        if (Request::current()->uri() == '/')
    	{       
			$this->redirect('terminal');
		}

		$this->template->loged = TRUE;
	}

	public function action_index()
    {
		$this->template->title = 'eTerminal';
  		$this->template->content = View::factory('terminal');		
	}
	
}
