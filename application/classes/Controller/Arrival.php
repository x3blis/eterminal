<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Arrival extends Controller_Template_Basic {

	public function action_index()
	{
		$this->template->title = __('Arrival');
  		$this->template->content = View::factory('arrival');
	}

}
