<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Invoices extends Controller_Template_Basic {

	public function action_index()
	{
		$this->template->title = __('Invoices');
  		$this->template->content = View::factory('clients/invoices');
	}
    
}
