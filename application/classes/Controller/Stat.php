<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Stat extends Controller_Template_Basic {

	public function action_index()
	{
		$this->template->title = __('Stat');
  		$this->template->content = View::factory('stat');
	}

}
