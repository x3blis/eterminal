<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Contracts extends Controller_Template_Basic {

	public function action_index()
	{
		$this->template->title = __('Contracts');
  		$this->template->content = View::factory('clients/contracts');
	}
    
}
