<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Log extends Controller_Template_Basic {

	public function action_index()
	{
		$this->template->title = __('Log');
  		$this->template->content = View::factory('log');
	}

}
