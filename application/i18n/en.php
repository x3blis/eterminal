<?php defined('SYSPATH') or die('No direct script access.');

return array(
    'Hello World' => 'Hello World',
    'en' => 'ENG',
    'ru' => 'RUS',
    'Create an account' => 'Create an account',
    'Login' => 'Login',
    'Logout' => 'Logout',
    'ABOUT'	=> 'ABOUT',
	'FAQ'	=> 'FAQ',
	':field must not be empty'		=> ':field must not be empty',
	'user.password.pwdneusr'		=> 'Must not be the same...',
);
