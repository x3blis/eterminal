<p><?php echo __('Terminal'); ?></p>
<div>
    <div id="search_sel">
      <span>События от</span><input type="text" id="date_from" value="" placeholder="<?php echo date('Y-m-d');?>" style="width:100px;margin:0 5px;" />
      <select id="hour_from" style="width:50px;">
      <?php 
      for($i=0;$i<24;$i++){
          	$hf = $i<10 ? '0'.$i : $i;
      		echo '<option value="'.$hf.'">'.$hf.'</option>'; 
      	}  
      ?> 	
      </select>
      <select id="minutes_from" style="width:50px;">
      <?php 
      for($i=0;$i<60;$i++){
              $hf = $i<10 ? '0'.$i : $i;
      		echo '<option value="'.$hf.'">'.$hf.'</option>'; 
      	}  
      ?> 	
      </select>
      <span>до</span><input type="text" id="date_to" value="" placeholder="<?php echo date('Y-m-d');?>" style="width:100px;margin:0 5px;" />
      <select id="hour_to" style="width:50px;">
      <?php
      for($i=23;$i>-1;$i--){
              $hf = $i<10 ? '0'.$i : $i;
      		echo '<option value="'.$hf.'">'.$hf.'</option>'; 
      	}   
      ?> 	
      </select>
      <select id="minutes_to" style="width:50px;">
      <?php 
      for($i=59;$i>-1;$i--){
              $hf = $i<10 ? '0'.$i : $i;
      		echo '<option value="'.$hf.'">'.$hf.'</option>'; 
      	}  
      ?> 	
      </select>
      <button id="double_search">Найти</button>
      <label>На терминале</label><input type="checkbox" id="onterminal" />
      <a href="#" id="search_reset">СБРОСИТЬ ПОИСК</a>
    </div>
    <div id="multi_search">
        <input type="text" value="" placeholder="Контейнер" id="container_search" />
        <select id="owner_select"><option value="">Владелец</option></select>
        <select id="position_select"><option value="">Место</option></select>
        <select id="state_select"><option value="">-</option></select>
        <select id="damage_select"><option value="">-</option></select>
        <select id="type_select"><option value="">Тип</option></select>
        <select id="capacity_select"><option value="">Вм-сть</option></select>
        <select id="size_select"><option value="">Раз-р</option></select>
        <a href="#" id="multi_search_submit" class="button icon approve">Искать</a>
    </div>
    
<table class="datatable" id="tableContainer">
    <thead>
		<tr>
            <th width="20">ID</th>
			<th width="100">Контейнер</th>
			<th width="120">Сток</th>			
			<th width="50">Место</th>
            <th width="50">E/F</th>
			<th width="70">A/U</th>
			<th width="70">Тип</th>
			<th width="70">Груз-ть</th>
			<th width="70">Размер</th>
            <th width="120">Прибыл</th> 
            <th width="120">Убыл</th>
            <th>Комментарий</th>
		</tr>
	</thead>       
    <tfoot>
		<tr>
            <th>ID</th>
			<th>Контейнер</th>
			<th>Сток</th>			
			<th>Место</th>
            <th>E/F</th>
			<th>A/U</th>
			<th>Тип</th>
			<th>Груз-ть</th>
			<th>Размер</th>
            <th>Прибыл</th> 
            <th>Убыл</th>
            <th>Комментарий</th>    
		</tr>
	</tfoot>
    <tbody>
		<tr>
			<td colspan="12" class="dataTables_empty">Загрузка</td>
		</tr>
	</tbody>	
</table>
<div class="clear"></div>
</div>