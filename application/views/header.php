<header>
    <!-- Logo -->
            <h1 id="logo">eTerminal</h1>
            <?php if(isset($loged)) { ?>  
            <!-- User info -->
            <div id="userinfo">
        
              <div class="intro">
                  <?php foreach(Kohana::$config->load('eterminal.language') as $lg) { ?>
                		<?php echo HTML::anchor('account/language/' . $lg, __($lg)); ?>&nbsp;
                	<?php } ?><br />
              <?php echo __('Hi');?>, 
               <?php echo ucfirst(stristr(Auth::instance()->get_user()->email, '@', true)); ?><br />
                <?php echo HTML::anchor('account/logout', __('Logout')); ?>
                
              </div>
            </div>	
        <?php } ?>
</header>