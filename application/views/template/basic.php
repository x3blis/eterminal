<!DOCTYPE HTML>
<html lang="<?php echo substr(I18n::$lang, 0, 2); ?>"> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<link rel="shortcut icon" type="image/x-icon" href="<?php echo URL::base(); ?>favicon.ico">
<title><?php echo $title ?></title>
<?php foreach ($styles as $file => $type) echo HTML::style($file, array('media' => $type)), PHP_EOL ?>
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>

    <div id="container" class="<?php if (!Auth::instance()->logged_in()) echo "logincontainer"; ?>">
        <?php include Kohana::find_file('views', 'header'); ?>
         <!-- The application "window" -->
        <div id="application">
      
        <?php include Kohana::find_file('views', 'nav/nav'); ?>
        
        <!-- The content -->
        <section id="content">

        <?php echo $content ?>
        
        </section>
        </div>
    
      <footer id="copyright"><?php echo __('Development &amp; support by');?> <a href="mailto:support@superuser.su">Aleksandr Korovkin</a> &copy;<?php echo date('Y');?></footer>
    </div>
<?php foreach ($scripts as $file) echo HTML::script($file), PHP_EOL ?>
</body>
</html>