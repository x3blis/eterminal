<?php 
if(isset($loged)) { 
    $menu = Kohana::$config->load('menu');
    $nav = '<nav id="primary"><ul>';
    $sub = '<nav id="secondary"><ul>';
    $icons = array('tables', 'pencil', 'newspaper', 'dashboard', 'modal', 'anchor', 'chart', 'gallery');
    $iterator = 0;
    foreach($menu as $menu_key=>$menu_val){
        $current = '';
        if(stristr(Request::current()->uri(), '/'.$menu_key)) $current =  "current";
        if(!is_array($menu_val)) {    
            $nav .= '<li class="'.$current.'"><a href="'.$menu_key.'"><span class="icon '.$icons[$iterator].'"></span>'.__($menu_val).'</a></li>';
            if(stristr(Request::current()->uri(), '/'.$menu_key))
            $sub .= '<li class="'.$current.'"><a href="'.$menu_key.'">'.__($menu_val).'</a></li>';
            $iterator++;
        }else{
            if(in_array(Request::current()->controller(), $menu_val))
                $current =  'current';
                $nav .= '<li class="'.$current.'"><a href="'.$menu_key.'"><span class="icon '.$icons[$iterator].'"></span>'.__(ucfirst($menu_key)).'</a></li>';
                $iterator++;
                if($current=='current'){
                    foreach($menu_val as $sub_key=>$sub_val){
                        $current = '';
                        if(stristr(Request::current()->uri(), $sub_key)) $current =  "current";
                        $sub .= '<li class="'.$current.'"><a href="'.$sub_key.'">'.__($sub_val).'</a></li>';
                    }
                }
           
        }
        
    }
    $nav .= '</ul></nav>';
    $sub .= '</ul></nav>';    
    echo $nav.$sub;
} else { ?>
            <nav id="secondary">
              <ul>
                <li class="<?php if(strstr(Request::current()->uri(), "login")) echo "current"; ?>"><a href="login"><?php echo __('Login');?></a></li>
                <li class="<?php if(strstr(Request::current()->uri(), "reset")) echo "current"; ?>"><a href="reset"><?php echo __('Reset password');?></a></li>
              </ul>
            </nav>
        <?php } ?>
        