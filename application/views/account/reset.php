<?php if(!isset($sent)) { ?>
    <?php if ($errors) { ?>
		<p class="message">Some errors were encountered, please check the details you entered.</p>
		<p>
		<ul class="errors">
		<?php foreach ($errors as $message): ?>
			<li><?php echo $message ?></li>
		<?php endforeach ?>
		</ul>
		</p>
	<?php } ?>
	<br /><br />
	      <p><?php echo __('Please send me a link to reset my password.'); ?></p>
	<?php echo Form::open();?>
    <section>
    <?php echo Form::label('email', 'Email') ?>
    <div><?php echo Form::input('email', $post['email']) ?></div>
    </section> 
    <section>
    <div style="text-align:right"><?php echo Form::submit(NULL, __('Reset'), array('class'=>'button primary')); ?></div>
    </section>
	<?php
	echo Form::close();
	?>
	   </div>
	</div>
<?php } else { ?>
    <br /><br />
	<p><span style="color:blue"><?php echo __('Check your email');?></span></p>
<?php } ?>