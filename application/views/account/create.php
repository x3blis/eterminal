<h1><?php echo __('Create an account'); ?></h1>
<?php if(!isset($sent)) { ?>
<?php if ($errors) { ?>
	<p class="message">Some errors were encountered, please check the details you entered.</p>
	<p>
	<ul class="errors">
	<?php foreach ($errors as $message): ?>
		<li><?php echo $message ?></li>
	<?php endforeach ?>
	</ul>
	</p>
<?php } ?>

<?php echo Form::open(NULL, array('id' => 'create', 'autocomplete' => 'off')); ?>
<fieldset>
	<dl>
		<dt><?php echo Form::label('email', 'Email') ?></dt>
		<dd><?php echo Form::input('email', $post['email'], array('id' => 'email')) ?><br /><span id="status"></span></dd>
	</dl>
	<input type="submit" name="Create">
</fieldset>

<?php echo Form::close(); ?>

<script>
var keyinterval;
var available = 0;

function checkavailability() {
	clearInterval(keyinterval);
	$("#status").html('<img align="absmiddle" src="<?php echo URL::base(); ?>assets/images/account/loader.gif" /> Checking availability...');
	$.ajax({
		type: "POST",
		url: "checkusername",
		data: "email=" + $("#email").val(),
		cache: false,
		async: true,
		dataType: "json",
		success: function(resultArray, textStatus, XMLHttpRequest)
		{
			if(resultArray['available']) {
				available = 1;
				$("#status").show();
				$("#status").html('<img align="absmiddle" src="<?php echo URL::base(); ?>assets/images/account/accepted.png" /> Available!');
                $('input[type=submit]').show();
			} else {
				available = 0;
				$("#status").show();
				$("#status").html('<img align="absmiddle" src="<?php echo URL::base(); ?>assets/images/account/notaccepted.png" /> Not available...');
			}
		},
		error: function(request, textStatus, errorThrown)
		{
			alert('error');
		}
	});
}

$(document).ready(function () {

	// focus email
	$("#email").focus();
    $('input[type=submit]').hide();
	// validation
	//$('#email').alphanumeric();
	
	// disable submit on submit
	$('form').submit(function(){
	    $('input[type=submit]').attr('disabled', 'disabled');
	});
	
	// check user availability
	$('#email').keyup(function() {
		var usr = $("#email").val();
		//var pwd = $("#password").val();

		
			if(usr.length >= 6) {
					clearInterval(keyinterval);
					keyinterval = setInterval( "checkavailability()", 1000);
			} else {
				$("#status").hide();
			}
		
	});

});
</script>
<?php } else { ?>
    Check your email
<?php } ?>