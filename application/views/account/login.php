
<?php if ($errors || $loginerrors) { ?>
	<p class="message">Some errors were encountered, please check the details you entered.</p>
	<p>
	<ul class="errors">
	<?php foreach ($errors as $message): ?>
	    <li><?php echo $message ?></li>
	<?php endforeach ?>
	<?php if(isset($loginerrors) && empty($errors)) { echo $loginerrors; } ?>
	</ul>
	</p>
<?php } ?>
 <br /><br />
<?php echo Form::open(); ?>
<section>
    <?php echo Form::label('username', __('Email')) ?>
    <div><?php echo Form::input('username', $post['username']) ?></div>
</section> 
<section>
    <?php echo Form::label('password', __('Password')) ?>
    <div><?php echo Form::password('password') ?></div>
<br /><br />
    <?php echo Form::label('remember', __('Remember')) ?>
    <div style="text-align:left;"><?php echo Form::checkbox('remember', NULL, !empty($post['remember'])) ?></div>
    <div style="text-align:right;"><?php echo Form::submit(NULL, __('Login'), array('class'=>'button primary')); ?></div>
</section>
<?php echo Form::close(); ?>