

function formattedTimeFromTimestamp(timestamp)
{
    var
        date = new Date(timestamp * 1000),
        year = date.getFullYear(),
        mon = parseInt(date.getMonth())+1,
        month = (mon < 10 ? '0' + mon : mon),
        day = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()),
        hours = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()),
        minutes = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()),
        //seconds = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds()),
        formattedTime = day + '.' + month + '.' + year + ' ' + hours + ':' + minutes;

    return formattedTime;
}

function getUnixTimestamp(date) {
       return new Date(date).getTime()/1000;
}

var iTable;
var in_format;
var events_period = '';

$(document).ready(function() {

        $('#double_search').on('click', function(event){
            event.preventDefault();
            var date_from = $("#date_from").val();
            var hour_from = $("#hour_from").val();
            var minutes_from = $("#minutes_from").val();
            var date_to = $("#date_to").val();
            var hour_to = $("#hour_to").val();
            var minutes_to = $("#minutes_to").val();     
            var from = date_from + ' ' + hour_from + ':' + minutes_from + ':00';
            var to = date_to + ' ' + hour_to + ':' + minutes_to + ':00';    
            var fromT = getUnixTimestamp(from);
            var toT = getUnixTimestamp(to);
            events_period = fromT + '~' + toT;
            iTable.fnDraw();
            
        });
        
        $('#search_reset').on('click', function(){
            event.preventDefault();
            events_period = '';
            iTable.fnFilterClear();
            //iTable.fnLengthChange( 100 );
            //$("#container_search").val('');
        });
        
        var opt_request = $.ajax({
          url: "services/containerOptions.php",
          type: "POST",
          data: '',
          dataType: "json",
          success: function(opt) {
              var owner_select = $('#owner_select');
              var position_select = $('#position_select');
              var state_select = $('#state_select');
              var damage_select = $('#damage_select');
              var type_select = $('#type_select');
              var capacity_select = $('#capacity_select');
              var size_select = $('#size_select');
              var index;
              var option;
              for (index = 0; index < opt.owner.length; ++index) {
                    option = '<option value="' + opt.owner[index] + '">' + opt.owner[index] + '</option>';
                    $(option).appendTo(owner_select);
              }
              for (index = 0; index < opt.position.length; ++index) {
                    option = '<option value="' + opt.position[index] + '">' + opt.position[index] + '</option>';
                    $(option).appendTo(position_select);
              }
              for (index = 0; index < opt.state.length; ++index) {
                    option = '<option value="' + opt.state[index] + '">' + opt.state[index] + '</option>';
                    $(option).appendTo(state_select);
              }
              for (index = 0; index < opt.damage.length; ++index) {
                    option = '<option value="' + opt.damage[index] + '">' + opt.damage[index] + '</option>';
                    $(option).appendTo(damage_select);
              }
              for (index = 0; index < opt.type.length; ++index) {
                    option = '<option value="' + opt.type[index] + '">' + opt.type[index] + '</option>';
                    $(option).appendTo(type_select);
              }
              for (index = 0; index < opt.capacity.length; ++index) {
                    option = '<option value="' + opt.capacity[index] + '">' + opt.capacity[index] + '</option>';
                    $(option).appendTo(capacity_select);
              }
              for (index = 0; index < opt.size.length; ++index) {
                    option = '<option value="' + opt.size[index] + '">' + opt.size[index] + '</option>';
                    $(option).appendTo(size_select);
              }
            }
        });
        
        


        iTable = $('#tableContainer').dataTable( {
                "sDom": '<"top"i>lfrtp',		
                "aLengthMenu": [[100, 50, 10], [ 100, 50, 10]],
                "iDisplayLength": 100,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "/eterminal/services/tableContainer.php",
                "sServerMethod":"POST",
                "fnServerData": function ( sUrl, aoData, fnCallback, oSettings ) {
                        if(/[0-9]{10}~[0-9]{10}/.exec( events_period ))
						aoData.push( { "name": "events_period", "value": events_period } );                        
						oSettings.jqXHR = $.ajax( {
                                "type": "post",
                                "url": sUrl,
                                "data": aoData,
                                "success": fnCallback,                               
                                "dataType": "json",
                                "cache": false
                        } );                        
				},
                /**"fnServerData": function( sUrl, aaData, fnCallback, oSettings ) {
                        oSettings.jqXHR = $.ajax( {
                                "url": sUrl,
                                "data": aaData,
                                "success": fnCallback,                               
                                "dataType": "jsonp",
                                "cache": false
                        } );
                }, */  
                "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
                    /* Append the grade to the default row class name */
                    if(aData['in_time'] !== '')
                    {   
                                //alert(in_format);
                               in_format = formattedTimeFromTimestamp(aData['in_time']);
                               $('td:eq(9)', nRow).html(in_format);

                    }
                    if(aData['out_time'] !== 0)
                    {   
                                //alert(in_format);
                               in_format = formattedTimeFromTimestamp(aData['out_time']);
                               $('td:eq(10)', nRow).html(in_format);

                    }else{
                        $('td:eq(10)', nRow).html('');
                    }
                    return nRow;
                },
                "aoColumns": [  
                            {
                                "mDataProp": "id"
                            },
                            {
                                "mDataProp": "container"
                            },
                            {
                                "mDataProp": "company"
                            },
                            {
                                "mDataProp": "position"
                            },
                            {
                                "mDataProp": "state"
                            },   
                            {
                                "mDataProp": "damage"
                            },
                            {
                                "mDataProp": "type"
                            },
                            {
                                "mDataProp": "capacity"
                            },
                            {
                                "mDataProp": "size"
                            },    
                            {
                                "mDataProp": "in_time"
                            },	
                            {
                                "mDataProp": "out_time"
                            },
                            {
                                "mDataProp": "comment"
                            }
                        ],
                "aaSorting": [[ 0, "desc" ]],
                    "aoColumnDefs": [ 
                        {"bVisible": false, "aTargets": [ ]},
                        {"bSortable": false, "aTargets": [11]},
                        {"bSearchable": false, "bSortable": false, "aTargets": []}
                    ] ,
                    "sPaginationType": "full_numbers",
                    "oLanguage":{    
                        "sLengthMenu":   "Показать _MENU_ записей",
                        "sZeroRecords":  "Записи отсутствуют.",
                        "sInfo":         "Записи от _START_ до _END_ из _TOTAL_ записей",
                        "sInfoEmpty":    "Записи с 0 до 0 из 0 записей",
                        "sInfoFiltered": "",
                        "sInfoPostFix":  "",
                        "sSearch":       "Поиск:",
                        "sUrl":          "",
                        "oPaginate": {
                            "sFirst": "Первая",
                            "sPrevious": "Предыдущая",
                            "sNext": "Следующая",
                            "sLast": "Последняя"
                        }
                    }
                
	} );
    
    $('#multi_search_submit').on('click', function(event){
            event.preventDefault();
            //iTable.fnLengthChange( 10 );
            var container_search = $('#container_search').val();
            var owner_search = $('#owner_select').val();
            var position_search = $('#position_select').val();
            var state_search = $('#state_select').val();
            var damage_search = $('#damage_select').val();
            var type_search = $('#type_select').val();
            var capacity_search = $('#capacity_select').val();
            var size_search = $('#size_select').val();
           // if(container_search=='Контейнер') container_search='';
            //iTable.fnMultiFilter( {"company": owner_search, "damage": damage_search});
            iTable.fnMultiFilter( { "container": container_search, "company": owner_search, "position": position_search, "state": state_search, "damage": damage_search, "type": type_search, "capacity": capacity_search, "size": size_search } );
    });
        
} );


//PLUGINS            
$.fn.dataTableExt.oApi.fnFilterClear  = function ( oSettings )
{
    /* Remove global filter */
    oSettings.oPreviousSearch.sSearch = "";
    oSettings.aaSorting = [];
      
    /* Sort display arrays so we get them in numerical order */
    oSettings.aiDisplay.sort( function (x,y) {
        return x-y;
    } );
    oSettings.aiDisplayMaster.sort( function (x,y) {
        return x-y;
    } );
      
    /* Remove the text of the global filter in the input boxes */
    if ( typeof oSettings.aanFeatures.f != 'undefined' )
    {
        var n = oSettings.aanFeatures.f;
        for ( var i=0, iLen=n.length; i<iLen ; i++ )
        {
            $('input', n[i]).val( '' );
        }
    }
      
    /* Remove the search text for the column filters - NOTE - if you have input boxes for these
     * filters, these will need to be reset
     */
    for ( var j=0, jLen=oSettings.aoPreSearchCols.length; j<jLen ; j++ )
    {
        oSettings.aoPreSearchCols[j].sSearch = "";
    }
    $("select option[value='']").attr('selected','selected');
    $( "input[type=checkbox]" ).attr('checked',false);
    
      
    /* Redraw */
    oSettings.oApi._fnReDraw( oSettings );
}; 

/* Plugin for multifiltering */
$.fn.dataTableExt.oApi.fnMultiFilter = function( oSettings, aData ) {
    for ( var key in aData )
    {
        if ( aData.hasOwnProperty(key) )
        {
            for ( var i=0, iLen=oSettings.aoColumns.length ; i<iLen ; i++ )
            {
                if( oSettings.aoColumns[i].mDataProp == key )
                {
                    /* Add single column filter */
                    oSettings.aoPreSearchCols[ i ].sSearch = aData[key];
                    break;
                }
            }
        }
    }
    this.oApi._fnDraw( oSettings );
};

//Usage
//oTable.fnMultiFilter( { "engine": "Gecko", "browser": "Cam" } );


//Plugin for changing length display
$.fn.dataTableExt.oApi.fnLengthChange = function ( oSettings, iDisplay )
{
    oSettings._iDisplayLength = iDisplay;
    oSettings.oApi._fnCalculateEnd( oSettings );
      
    /* If we have space to show extra rows (backing up from the end point - then do so */
    if ( oSettings._iDisplayEnd == oSettings.aiDisplay.length )
    {
        oSettings._iDisplayStart = oSettings._iDisplayEnd - oSettings._iDisplayLength;
        if ( oSettings._iDisplayStart < 0 )
        {
            oSettings._iDisplayStart = 0;
        }
    }
      
    if ( oSettings._iDisplayLength == -1 )
    {
        oSettings._iDisplayStart = 0;
    }
      
    oSettings.oApi._fnDraw( oSettings );
      
    if ( oSettings.aanFeatures.l )
    {
        $('select', oSettings.aanFeatures.l).val( iDisplay );
    }
};

//Usage
//iTable.fnLengthChange( 100 );


//Plugin for Ajax reloading
$.fn.dataTableExt.oApi.fnReloadAjax = function ( oSettings, sNewSource, fnCallback, bStandingRedraw )
{
    if ( sNewSource !== undefined && sNewSource !== null ) {
        oSettings.sAjaxSource = sNewSource;
    }
 
    // Server-side processing should just call fnDraw
    if ( oSettings.oFeatures.bServerSide ) {
        this.fnDraw();
        return;
    }
 
    this.oApi._fnProcessingDisplay( oSettings, true );
    var that = this;
    var iStart = oSettings._iDisplayStart;
    var aData = [];
 
    this.oApi._fnServerParams( oSettings, aData );
 
    oSettings.fnServerData.call( oSettings.oInstance, oSettings.sAjaxSource, aData, function(json) {
        /* Clear the old information from the table */
        that.oApi._fnClearTable( oSettings );
 
        /* Got the data - add it to the table */
        var aData =  (oSettings.sAjaxDataProp !== "") ?
            that.oApi._fnGetObjectDataFn( oSettings.sAjaxDataProp )( json ) : json;
 
        for ( var i=0 ; i<aData.length ; i++ )
        {
            that.oApi._fnAddData( oSettings, aData[i] );
        }
         
        oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
 
        that.fnDraw();
 
        if ( bStandingRedraw === true )
        {
            oSettings._iDisplayStart = iStart;
            that.oApi._fnCalculateEnd( oSettings );
            that.fnDraw( false );
        }
 
        that.oApi._fnProcessingDisplay( oSettings, false );
 
        /* Callback user function - for event handlers etc */
        if ( typeof fnCallback == 'function' && fnCallback !== null )
        {
            fnCallback( oSettings );
        }
    }, oSettings );
};
//Usage: iTable.fnReloadAjax();