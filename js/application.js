$('document').ready(function() {
  
  /**
   * Make sure the background gradient reaches
   * the bottom of the page.
   */
  function initBackground() {
    if($('#container').height() < window.innerHeight) {
      $('#container').height(window.innerHeight);
    }
  }
  
  initBackground();
  
});